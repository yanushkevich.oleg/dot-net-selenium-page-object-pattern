﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumPageObject.Pages;

IWebDriver driver = new ChromeDriver();

try
{
    var indexPage = new IndexPage(driver);

    indexPage.Open().Search("Automation");
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
finally
{
    driver?.Quit();
}