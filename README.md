# Selenium first script

**Estimated reading time**: 15 minutes

## Story Outline
Selenium is a one of the most popular tool for GUI testing.
This story is about how to create and run your first script with using [Selenium WebDriver](https://www.selenium.dev/).

## Story Organization
**Story Branch**: main
> `git checkout main`

Tags: .NET Selenium